#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# FILE:         app.py
# ORIGINATOR:   CHEN, Kedwin <kedwinchen.noreply+git@gmail.com>
# LICENSE:      MIT
# DESCRIPTION:  A Flask application, vulnerable to command injection
###############################################################################


import flask
import wtforms
import subprocess


app = flask.Flask(__name__)


class SimpleForm(wtforms.Form):
    directory = wtforms.TextField("Directory: ",
                                  validators=[wtforms.validators.required()])


@app.route("/ls", methods=["GET", "POST"])
def list():
    """ lists the contents of a directory """
    form = SimpleForm(flask.request.form)
    res = None
    form_dir = None

    if flask.request.method == "POST":
        if form.validate():
            form_dir = flask.request.form["directory"]
            try:
                res = subprocess.check_output(f"ls -halZ {form_dir}",
                                              shell=True).decode('utf-8')
                res = res.replace("\n", "<br>")
            except subprocess.CalledProcessError:
                res = f"Ooops, invalid directory name ({form_dir}) provided!"
        else:
            flask.flash("Directory name required")

    return flask.render_template("directory.html.j2",
                                 form=form, result=res, dirname=form_dir)


@app.route("/")
def hello() -> str:
    """ Hello world """
    return "Hello, world!"


if __name__ == '__main__':
    app.run()
